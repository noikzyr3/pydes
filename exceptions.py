"""Custom bot Exceptions"""


class BadValueIniFile(Exception):
    pass


class InsufficientFunds(Exception):
    pass


class UnexpectedOrderStatus(Exception):
    pass


class OperationNotAvailable(Exception):
    pass


class LimitTimeOut(Exception):
    pass
