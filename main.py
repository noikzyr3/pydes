"""Main execution rutine for the bot"""
import time

from twisted.internet import task
from twisted.internet import reactor

import bot


def main_raw():
    while True:
        bot_class = bot.BotController()
        bot_class.run()
        time.sleep(60)


def main_twisted():
    # https://stackoverflow.com/questions/474528/what-is-the-best-way-to-repeatedly-execute-a-function-every-x-seconds-in-python
    timeout = 60.0  # Sixty seconds
    bot_class = bot.BotController()

    l = task.LoopingCall(bot_class.run)
    l.start(timeout)  # call every sixty seconds

    reactor.run()


if __name__ == "__main__":
    main_raw()
