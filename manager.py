"""Class that connects with the external API"""
import requests

import constants
import settings


class Manager(object):
    def __init__(self, config):
        self.config = config
        self.url = settings.server
        self.benefits = None
        self.status = None
        self.funds = None
        self.last_price = None
        self.stop_loss_order = None
        self.take_profit_order = None

        self.status_url = 'http://{}/api/status'.format(self.url)

    def check_rights(self):
        """Check if the API has withdraw rights, if it doesn't have, stop the program"""
        return True

    def check_benefits(self):
        """If benefits are bigger than benefit limit, transfer % and reset the counter"""
        # Get percentage

        # If percentage is 0: return

    def get_benefits(self):
        # TODO
        return self.benefits

    def get_funds(self):
        if self.funds is None:
            self._get_status()
        return self.funds

    def get_status(self):
        if self.status is None:
            self._get_status()
        return self.status

    def _get_status(self):
        params = {'pair': self.config['bot']['pair'],
                  'simulation': False}  # self.config.getboolean('bot', 'simulation')}
        headers = self.security_headers()

        r = requests.get(self.status_url, params=params, headers=headers)
        r.raise_for_status()

        r_json = r.json()
        self.status = r_json['status'] if r_json['status'] != '' else constants.READY_TO_BUY
        self.funds = r_json['funds']
        self.last_price = r_json['last_price']
        self.stop_loss_order = r_json['stop_loss_order']
        self.take_profit_order = r_json['take_profit_order']
        print('{} - {} - {}'.format(self.config['bot']['pair'], self.status, self.funds))

    def pay_benefits(self):
        # TODO
        pass

    def security_headers(self):
        return {'license': self.config['bot']['license']}

    def update_benefits(self, amount):
        # TODO
        self.benefits = amount

    def update_status(self, funds=None):
        data = {'pair': self.config['bot']['pair'],
                'simulation': False,  # self.config.getboolean('bot', 'simulation'),
                'status': self.status,
                'last_price': self.last_price,
                'stop_loss_order': self.stop_loss_order}
        if funds:
            data['funds'] = funds
            print('Updating funds: {}'.format(funds))
        headers = self.security_headers()

        print('Updating status: {}'.format(self.status))
        r = requests.post(self.status_url, data=data, headers=headers)
        r.raise_for_status()


class ManagerSimulation(object):
    def __init__(self):
        self.url = settings.server
        self.benefits = 0.0
        self.status = constants.READY_TO_BUY

    def check_security(self, key, license):
        # TODO
        return True

    def check_rights(self):
        """Check if the API has withdraw rights, if it doesn't have, stop the program"""
        return True

    def check_benefits(self):
        """If benefits are bigger than benefit limit, transfer % and reset the counter"""
        # Get percentage

        # If percentage is 0: return

    def get_benefits(self):
        # TODO
        return self.benefits

    def get_status(self):
        # TODO
        return self.status

    def pay_benefits(self):
        # TODO
        pass

    def update_benefits(self, amount):
        # TODO
        self.benefits = amount

    def update_status(self, status):
        self.status = status
