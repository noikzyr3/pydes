"""Basic scalping strategy"""
import constants
from indicators import sma
from indicators import stochastic
from strategies import base


class SimpleScalpingStrategy(base.StrategyBase):
    """Basic strategy with SMA indicator"""

    indicators = {
        'sma': (sma.SimpleMovingAverage, (5, 9)),
        'stochastic': (stochastic.Stochastic, (10,)),
    }

    def calculate(self):
        print('sma: {} - stochastic: {}'.format(self.result['sma'], self.result['stochastic']))
        if (self.result['sma'] == constants.BUY
                and self.result['stochastic'] == constants.BUY):
            return constants.BUY
        if self.result['sma'] == constants.SELL and self.result['stochastic'] == constants.SELL:
            return constants.SELL
        return constants.NOOP
