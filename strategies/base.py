"""Strategy Base"""
import abc


class StrategyBase(metaclass=abc.ABCMeta):
    """Base with basic input and output for a strategy"""

    indicators = dict()

    def __init__(self, candles, config):
        self.dataframe = candles
        self.config = config
        self.result = dict()

    def execute(self):
        """Execute the strategy and return the new status"""
        for key, indicator_tuple in self.indicators.items():
            indicator_class, values = indicator_tuple
            indicator = indicator_class(self.dataframe, self.config)
            self.result[key] = indicator.execute(*values) if values else indicator.execute()
        return self.calculate()

    @abc.abstractmethod
    def calculate(self):
        """Get the indicator results and decide what to do"""
