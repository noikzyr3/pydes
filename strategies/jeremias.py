"""Jeremías strategy"""
import constants
from strategies import base
from indicators import ema
from indicators import sma_volume


class TitoJereStrategy(base.StrategyBase):
    """Jeremías youtube scalping strategy with ema"""

    indicators = {'ema': (ema.ExponentialMovingAverage, (3, 4)),
                  'sma_volume': (sma_volume.SimpleMovingAverageVolume, (4,))}

    def calculate(self):
        if self.result['ema'] == constants.BUY and self.result['sma_volume'] == constants.BUY:
            return constants.BUY
        if self.result['ema'] == constants.SELL:
            return constants.SELL
        return constants.NOOP
