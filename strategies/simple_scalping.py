"""Basic scalping strategy"""
import constants
from indicators import sma
from indicators import sma_volume
from indicators import standard_deviation
from indicators import bollinger_bands
from indicators import rsi
from indicators import stochastic
from strategies import base


class SimpleScalpingStrategy(base.StrategyBase):
    """Basic strategy with SMA indicator"""

    indicators = {
        'sma': (sma.SimpleMovingAverage, (5, 9)),
        # 'sma_volume': (sma_volume.SimpleMovingAverageVolume, (10,)),
        # 'sd': standard_deviation.StandardDeviation,
        # 'rsi': (rsi.RSI, (9,)),
        'stochastic': (stochastic.Stochastic, (10,)),
        # 'bb': bollinger_bands.BollingerBands,
    }

    def calculate(self):
        print('sma: {} - stochastic: {}'.format(self.result['sma'], self.result['stochastic']))
        if (self.result['sma'] == constants.BUY
                # and self.result['sma_volume'] == constants.BUY
                and self.result['stochastic'] == constants.BUY
                ):
            return constants.BUY
        if self.result['sma'] == constants.SELL and self.result['stochastic'] == constants.SELL:
            return constants.SELL
        return constants.NOOP
