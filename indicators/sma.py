"""Moving average indicator"""
from indicators import base
import constants


class SimpleMovingAverage(base.BaseIndicator):
    """Moving average indicator.

    Indicator Type: Trend"""

    name = 'sma'
    indicator_type = constants.TREND

    def evaluate(self, sma5, sma8, sma13):
        """Evaluates if exist any signal for buy or sell.

        - If sma5 < sma8 -> SELL
        - If sma5 > sma8 -> BUY

         TO IMPROVE: Evaluate inclination from previous data
        """
        if sma8.values[-1] > sma5.values[-1]:
            return constants.SELL
        if sma8.values[-1] < sma5.values[-1]:
            return constants.BUY
        return constants.NOOP

    def execute(self, ma_min=6, ma_mid=9, ma_max=13):
        sma5 = self.moving_average(self.df, ma_min)
        sma8 = self.moving_average(self.df, ma_mid)
        sma13 = self.moving_average(self.df, ma_max)

        return self.evaluate(sma5, sma8, sma13)

    @staticmethod
    def moving_average(df, period, column='close'):
        sma = df[column].rolling(window=period, min_periods=period - 1).mean()
        return sma
