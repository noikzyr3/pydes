"""Exponential Moving average indicator"""
from indicators import base
import constants


class ExponentialMovingAverage(base.BaseIndicator):
    def evaluate(self, ema_low, ema_high):
        if ema_high.values[-1] > ema_low.values[-1]:
            return constants.SELL
        if ema_high.values[-1] < ema_low.values[-1]:
            return constants.BUY
        return constants.NOOP

    def execute(self, low=3, high=7):
        ema_low = self.ema(low)
        ema_high = self.ema(high)

        return self.evaluate(ema_low, ema_high)

    def ema(self, period, column='close'):
        ema = self.df[column].ewm(span=period, min_periods=period - 1).mean()
        return ema

    def moving_average(self, period, column='close'):
        sma = self.df[column].rolling(window=period, min_periods=period - 1).mean()
        return sma
