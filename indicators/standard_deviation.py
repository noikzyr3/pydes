"""Standard deviation indicator"""
from indicators import base
from indicators import sma


class StandardDeviation(base.BaseIndicator):
    def evaluate(self, sd, large_sma):
        if sd.values[-1] > large_sma.values[-1]:
            return True
        else:
            return False

    def execute(self, sd=10):
        return self.evaluate(self.standard_deviation(sd),
                             sma.SimpleMovingAverage.moving_average(self.df, 100))

    def standard_deviation(self, period, column='close'):
        sd = self.df[column].rolling(window=period, min_periods=period - 1).std()
        return sd
