"""Relative strenght index indicator"""
from indicators import base
import constants


class BollingerBands(base.BaseIndicator):
    def evaluate(self, upper, lower, current):
        if current < lower:
            return constants.BUY
        if current > upper:
            return constants.SELL
        return constants.NOOP

    def execute(self):
        return self.evaluate(*self.bollinger_bands())

    def bollinger_bands(self, period=14, column='close'):
        sma = self.df[column].rolling(window=period, min_periods=period - 1).mean()
        std = self.df[column].rolling(window=period, min_periods=period - 1).std()

        up = (sma + (std * 2)).values[-1]
        lower = (sma - (std * 2)).values[-1]
        current = self.df['close'].values[-1]
        return up, lower, current
