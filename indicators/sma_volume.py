"""Exponential Moving average indicator"""
from indicators import base
from indicators import sma
import constants


class SimpleMovingAverageVolume(base.BaseIndicator):
    def evaluate(self, volume, current_volume):
        if current_volume > volume.values[-1]:
            return constants.BUY
        return constants.NOOP

    def execute(self, v=9, column='volume'):
        volume = sma.SimpleMovingAverage.moving_average(self.df, v, column)
        current_volume = self.df[column][len(self.df) - 1]

        return self.evaluate(volume, current_volume)
