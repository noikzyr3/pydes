"""Stochastic indicator"""
from indicators import base
import constants


class Stochastic(base.BaseIndicator):
    def evaluate(self, s):
        if s > 80:
            return constants.SELL
        if s < 20:
            return constants.BUY
        return constants.NOOP

    def execute(self, period=14):
        return self.evaluate(self.stochastic(period))

    def stochastic(self, period, column='close'):
        # formula from: https://www.investopedia.com/terms/s/stochasticoscillator.asp
        c = float(self.df[column].values[-1])
        low_value = float(self.df[column].values[-period:].min())
        max_value = float(self.df[column].values[-period:].max())

        try:
            k = 100 * (c - low_value) / (max_value - low_value)
        except ZeroDivisionError:
            print('stoc ZeroDivisionError: c:{} - low:{} - max:{}'.format(c,
                                                                          low_value,
                                                                          max_value))
            k = 0
        print('stoc: {}'.format(k))
        return k

