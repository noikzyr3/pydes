"""Base class for indicators"""
import abc


class BaseIndicator(metaclass=abc.ABCMeta):
    """Base class with basic functions for indicators"""

    Name = 'Base'
    indicator_type = None

    def __init__(self, df, config, *args, **kwargs):
        self.df = df
        self.config = config

    @abc.abstractmethod
    def evaluate(self, *args, **kwargs):
        """Indicator logic evaluation in order to know if it shows a signal"""

    @abc.abstractmethod
    def execute(self,  *args, **kwargs):
        """Algorithm calculation"""
