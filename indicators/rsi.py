"""Relative strenght index indicator"""
from indicators import base
import constants


class RSI(base.BaseIndicator):
    def evaluate(self, rsi):
        if rsi.values[-1] > 70:
            return constants.SELL
        if rsi.values[-1] < 30:
            return constants.BUY
        return constants.NOOP

    def execute(self, n=10):
        return self.evaluate(self.rsi(n))

    def rsi(self, period=14, column='close'):
        delta = self.df[column].diff()
        up, down = delta.copy(), delta.copy()

        up[up < 0] = 0
        down[down > 0] = 0

        r_up = up.ewm(com=period - 1,  adjust=False).mean()
        r_down = down.ewm(com=period - 1, adjust=False).mean().abs()

        rsi = 100 - 100 / (1 + r_up / r_down)

        return rsi
