"""Bot Controller"""
import configparser

from apis import binance
from strategies import simple_scalping, jeremias
import constants
import exceptions
import manager


class BotController(object):
    """Manage the flux of the program execution"""

    def __init__(self, config_file='status.ini'):
        self.config = configparser.ConfigParser()
        self.config.read(config_file)

        self.manager = manager.Manager(self.config)

        self.exchange = self.get_exchange()

    def run(self):
        """Main execution method"""
        self.manager.check_benefits()

        self.check_status()

        candles = self.exchange.get_candles()
        strategy = self.get_strategy(candles, self.config)
        action = strategy.execute()

        try:
            self.execute_action(action)
        except exceptions.LimitTimeOut:
            # TODO: What happens if partially limit?
            print('Limit Timeout reached. Aborted.')

    def get_exchange(self):
        return self.get_exchange_normal()

    def get_exchange_normal(self):
        """Factory method for exchanges"""
        if self.config['bot']['exchange'].lower() == 'binance':
            return binance.Binance(self.config, self.manager)
        raise exceptions.BadValueIniFile('Exchange not available.')

    def get_strategy(self, exchange, status):
        """Factory method for strategy"""
        if self.config['bot']['strategy'].lower() == 'simple scalping':
            return simple_scalping.SimpleScalpingStrategy(exchange, status)
        if self.config['bot']['strategy'].lower() == 'jeremias':
            return jeremias.TitoJereStrategy(exchange, status)
        if self.config['bot']['strategy'].lower() == '':
            return simple_scalping.SimpleScalpingStrategy(exchange, status)
        raise exceptions.BadValueIniFile('Strategy not available.')

    def execute_action(self, action):
        """Factory method for actions"""
        if action == constants.BUY:
            buy_result = self.perform_buy()
            if buy_result:
                price, stop_loss_id = buy_result
                self.manager.last_price = price
                self.manager.stop_loss_order = stop_loss_id
                self.manager.status = constants.WAITING_FOR_SELL
                self.manager.update_status()
        if action == constants.SELL:
            new_amount = self.perform_sell()
            self.manager.status = constants.READY_TO_BUY
            self.manager.update_status(new_amount)
        if action == constants.NOOP:
            self.perform_noop()

    def perform_buy(self):
        if self.manager.get_status() == constants.READY_TO_BUY:
            return self.exchange.buy()

    def perform_sell(self):
        if self.manager.get_status() == constants.WAITING_FOR_SELL:
            return self.exchange.sell()

    def perform_noop(self):
        """It does nothing"""
        pass

    def check_status(self):
        """If status is ready to sell but there is not funds means that stop loss
        has triggered."""
        if self.manager.stop_loss_order and self.manager.get_status() == constants.WAITING_FOR_SELL:
            self.exchange.manage_stop_loss(self.manager.stop_loss_order)
