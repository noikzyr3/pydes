"""Base class for all api wrappers"""
import abc
import time

import constants
import exceptions
import settings


class Base(metaclass=abc.ABCMeta):
    """Base class with mandatory methods for every api wrapper"""

    exchange = None

    def __init__(self, config, manager, **kwargs):
        self.config = config
        self.manager = manager
        self.status = manager.get_status()
        self.funds = False if not bool(manager.funds) else manager.funds

    @property
    def amount(self):
        start = False
        if self.status == constants.READY_TO_BUY:
            # TODO: Can't do that. Get all funds of ETH pool will let other bots without funds.
            if not self.funds:
                self.funds = self.get_funds()
                start = True
            if start and self.funds >= self.buy_amount_start:
                self.funds = self.buy_amount_start
                return self.buy_amount_start
            if self.funds < self.buy_amount_min:
                raise exceptions.InsufficientFunds('Funds are below the minimum')
            if self.funds > self.buy_amount_max:
                return self.buy_amount_max
        return self.funds

    @property
    def buy_amount_start(self):
        return float(self.config['bot']['buy_amount_start'])

    @property
    def buy_amount_min(self):
        return float(self.config['bot']['buy_amount_min'])

    @property
    def buy_amount_max(self):
        return float(self.config['bot']['buy_amount_max'])

    @property
    def buy_percentage(self):
        return float(self.config['bot']['buy_percentage'])

    @property
    def interval(self):
        return self.format_interval(int(self.config['bot']['interval']))

    @property
    def market_price(self):
        return self.config.getboolean('bot', 'market_price')

    @property
    def max_candle(self):
        return int(self.config['bot']['max_candle'])

    @property
    def pair(self):
        return self.format_pair(self.config['bot']['pair'])

    @property
    def sell_percentage(self):
        return float(self.config['bot']['sell_percentage'])

    @property
    def stop_loss_percentage(self):
        return float(self.config['bot']['stop_loss_percentage'])

    @property
    def stop_loss_price_percentage(self):
        return float(self.config['bot']['stop_loss_price_percentage'])

    @property
    def weak_coin(self):
        return self.pair.replace('ETH', '')

    def buy(self):
        if self.market_price:
            self.buy_market()
        else:
            # THIS IS TEMPORARY
            result = self.perform_action_limit(self.buy_limit,
                                               self.get_buy_quantity,
                                               self.get_buy_price)
            print(result)
            prices, qty = result
            order_id = self.set_stop_loss_limit(min([price for price in prices if price != 0]))
            prices_not_zero = [p for p in prices if p != 0]
            mean_price = sum(prices_not_zero) / len(prices_not_zero)

            return round(mean_price, self.get_price_precision()), order_id

    @abc.abstractmethod
    def buy_limit(self, quantity, price):
        """Buy tokens with limit price.

        :return order_id"""

    @abc.abstractmethod
    def buy_market(self, qty=None):
        """Buy tokens at market price"""

    @abc.abstractmethod
    def calculate_new_qty(self, order):
        """Calculates new qty from canceled order"""

    @abc.abstractmethod
    def cancel_order(self, order_id):
        """Cancel the given order"""

    @abc.abstractmethod
    def check_order_first_position(self, price):
        """Check if our order is still the first"""

    @abc.abstractmethod
    def check_order_status_canceled(self, order, *args, **kwargs):
        """Check status cancelled of an order, returns boolean

        :return: Tuple(order, boolean)"""

    @abc.abstractmethod
    def check_order_status_filled(self, order, *args, **kwargs):
        """Check status filled of an order, returns boolean

        :return: Tuple(order, boolean)"""

    @abc.abstractmethod
    def check_order_status_partially_filled(self, order, *args, **kwargs):
        """Check status partially filled of an order, returns boolean

        :return: Tuple(order, boolean)"""

    @abc.abstractmethod
    def check_order_status_new(self, order, *args, **kwargs):
        """Check status filled of an order, returns boolean

        :return: Tuple(order, boolean)"""

    @abc.abstractmethod
    def format_interval(self, interval):
        """Format interval for specific exchange"""

    @abc.abstractmethod
    def format_pair(self, pair):
        """Format pair for specific exchange"""

    @abc.abstractmethod
    def get_candles(self, *args, **kwargs):
        """Should return a pandas dataframe"""

    def get_buy_price(self):
        """Calculates the qty to buy from book"""
        book = self.get_order_book()
        return round(book['buy'] * (1 + (self.buy_percentage / 100)),
                     self.get_price_precision())

    def get_buy_quantity(self, price):
        print('qty: {}, precision: {}'.format((self.amount / price), self.get_qty_precision()))
        return round((self.amount / price), self.get_qty_precision())

    @abc.abstractmethod
    def get_minimum_notional(self):
        """Gets the minimum quantity for a transaction"""

    @abc.abstractmethod
    def get_order(self, order_id):
        """Gets the order"""

    @abc.abstractmethod
    def get_price_precision(self):
        """Gets the minimum unit for a pair."""

    @abc.abstractmethod
    def get_qty_precision(self):
        """A pair could have a minimum unit, if the min is 0.1 and we send 0.11 it will fails."""

    def get_sell_price(self):
        """Calculates the qty to sell from book"""
        book = self.get_order_book()
        return book['sell'] - (book['sell'] * (self.sell_percentage / 100))

    def get_sell_quantity(self, *args, **kwargs):
        """Calculates the quantity to sell.

        Return tuple: quantity
        """
        qty = self.get_funds(asset=self.weak_coin)
        qty = round(qty, self.get_qty_precision())
        print('qty: {}, precision: {}'.format(qty, self.get_qty_precision()))
        return qty

    def get_stop_price(self, buy_price):
        """Calculates the price for a stop limit by percentage in config"""
        return round(buy_price - (buy_price * self.stop_loss_percentage / 100),
                     self.get_price_precision())

    def get_stop_limit_price(self, stop_price):
        """Calculates the price that will be set for the order limit"""
        return round(stop_price - (stop_price * self.stop_loss_price_percentage),
                     self.get_price_precision())

    @abc.abstractmethod
    def get_funds(self, asset='ETH'):
        """Get the current funds"""

    @abc.abstractmethod
    def get_order_book(self, *args, **kwargs):
        """Return the last prices of order book.

        :return {'buy': 123.1, 'sell': 132.2}"""
        # TODO: It should return the quantity too

    def manage_stop_loss(self, order_id):
        # TODO: Get the current price, if it's less than the stop loss, sell instant
        order = self.get_order(order_id)
        if self.check_order_status_filled(order):
            print('ATENTION: Stop Loss Triggered and completed.')
            self.manager.status = constants.READY_TO_BUY
            self.manager.stop_loss_order = None
            self.manager.update_status()
        elif self.check_order_status_partially_filled(order):
            self.cancel_order(order_id)
            self.sell_market()  # It sells all the remaining amount
            self.manager.status = constants.READY_TO_BUY
            self.manager.stop_loss_order = None
            self.manager.update_status()
        elif self.check_order_status_new(order):
            return
        else:
            raise exceptions.UnexpectedOrderStatus

    def perform_action_limit(self,
                             limit_action,
                             qty_action,
                             price_action,
                             quantity=None,
                             prices=None,
                             qtys=None,
                             timeout=None):
        """Reusable action for limit orders"""
        if prices is None and qtys is None:
            prices = list()
            qtys = list()
        price = price_action()
        if not quantity:
            quantity = qty_action(price)
        order_id = limit_action(quantity, price)

        now = int(time.time())
        if not timeout:
            timeout = now + settings.limit_timeout_seconds

        while time.time() < timeout:
            time.sleep(settings.limit_check_seconds)

            order = self.get_order(order_id)
            filled = self.check_order_status_filled(order)
            if filled:
                prices.append(price)
                qtys.append(quantity)
                return prices, qtys
            else:
                if not self.check_order_first_position(price):
                    if not self.cancel_order(order_id):
                        return prices, qtys
                    new_qty = self.calculate_new_qty(order)
                    prices.append(price)
                    qtys.append(quantity - new_qty)
                    if (new_qty * price) < self.get_minimum_notional():
                        print('Minimum Qty ({}) reached: {}'.format(self.get_minimum_notional(),
                                                                    (new_qty * price)))
                        return prices, qtys

                    return self.perform_action_limit(limit_action,
                                                     qty_action,
                                                     price_action,
                                                     new_qty,
                                                     prices,
                                                     qtys,
                                                     timeout)
        else:
            self.cancel_order(order_id)
            raise exceptions.LimitTimeOut

    def sell(self):
        if self.manager.stop_loss_order:
            self.cancel_order(self.manager.stop_loss_order)
            self.manager.stop_loss_order = None
            self.manager.update_status()
        if self.market_price:
            amount = self.sell_market()
        else:
            # THIS IS TEMPORARY
            result = self.perform_action_limit(self.sell_limit,
                                               self.get_sell_quantity,
                                               self.get_sell_price)
            prices, qty = result
            print('prices: {}'.format(prices))
            print('qtys: {}'.format(qty))
            amount = sum([x[0] * x[1] for x in zip(prices, qty)])

        print('amount: {}'.format(amount))
        return round(amount, self.get_qty_precision())

    @abc.abstractmethod
    def sell_limit(self, quantity, price):
        """Sell with limit price"""

    @abc.abstractmethod
    def sell_market(self):
        """Sell at market price"""

    @abc.abstractmethod
    def set_stop_loss(self, buy_price):
        """Set the stop loss with a percentage given in config"""

    @abc.abstractmethod
    def set_stop_loss_limit(self, buy_price):
        """Set the stop loss limit with a percentage given in config"""
