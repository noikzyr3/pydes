"""Binance API wrapper"""
import pandas

from binance.client import Client
from binance import enums
from binance import exceptions as b_exceptions

from apis import base
import exceptions


class Binance(base.Base):
    """API wrapper for Binance"""

    exchange = 'binance'

    def __init__(self, config, manager, **kwargs):
        super(Binance, self).__init__(config, manager, **kwargs)
        self.client = Client(self.config[self.exchange]['binance_api_key'],
                             self.config[self.exchange]['binance_api_secret'])
        self.filters = None

    def buy_limit(self, quantity, price):
        price = round(price, self.get_price_precision())
        quantity = round(quantity, self.get_qty_precision())
        print('Buy Limit - price: {} - quantity: {}'.format(price, quantity))
        order = self.client.order_limit_buy(symbol=self.pair,
                                            quantity=quantity,
                                            price=price)
        print('Order ID: {}'.format(order['orderId']))
        return order['orderId']

    def buy_market(self, qty=None):
        qty = self.calculate_quantity_buy_market_price() if not qty else qty
        order = self.client.order_market_buy(
            symbol=self.pair,
            quantity=qty
        )
        return order

    def calculate_quantity_buy_market_price(self):
        """Takes de first sell order from the order book.
        The price is orientative due high fluctuation."""
        book = self.get_order_book()

        return round(self.amount / book['sell'], self.get_qty_precision())

    def calculate_new_qty(self, order):
        # new_qty = qty - float(order['executedQty'])
        new_qty = float(order['origQty']) - float(order['executedQty'])
        print('New Qty: {}'.format(new_qty))

        return new_qty

    def cancel_order(self, order_id):
        try:
            self.client.cancel_order(symbol=self.pair,
                                     orderId=order_id)
        except b_exceptions.BinanceAPIException as exc:
            order = self.get_order(order_id)
            if not self.check_order_status_filled(order):
                raise exc
            return False
        print('Canceling order with ID: {}'.format(order_id))
        return True

    def check_order_first_position(self, price):
        book = self.get_order_book()

        return round(price, self.get_price_precision()) in book.values()

    def check_order_status_canceled(self, order, *args, **kwargs):
        """Check de order status if it's cancelled."""
        print('Order status check cancelled: {}'.format(order['status']))
        if order['status'] == enums.ORDER_STATUS_CANCELED:
            return True
        return False

    def check_order_status_filled(self, order, *args, **kwargs):
        """Check de order status that can be: NEW, FILLED, PARTIALLY_FILLED,
        CANCELED, REJECTED, EXPIRED."""
        print('Order status check filled: {}'.format(order['status']))
        if order['status'] == enums.ORDER_STATUS_FILLED:
            return True
        return False

    def check_order_status_partially_filled(self, order, *args, **kwargs):
        """Check de order status if it's partially filled."""
        print('Order status check partially: {}'.format(order['status']))
        if order['status'] == enums.ORDER_STATUS_PARTIALLY_FILLED:
            return True
        return False

    def check_order_status_new(self, order, *args, **kwargs):
        """Check de order status if it's new."""
        print('Order status check new: {}'.format(order['status']))
        if order['status'] == enums.ORDER_STATUS_NEW:
            return True
        return False

    def format_interval(self, interval):
        if interval == 1:
            return enums.KLINE_INTERVAL_1MINUTE
        if interval == 3:
            return enums.KLINE_INTERVAL_3MINUTE
        if interval == 5:
            return enums.KLINE_INTERVAL_5MINUTE
        if interval == 15:
            return enums.KLINE_INTERVAL_15MINUTE
        if interval == 30:
            return enums.KLINE_INTERVAL_30MINUTE
        if interval == 60:
            return enums.KLINE_INTERVAL_1HOUR
        raise exceptions.BadValueIniFile('Interval not accepted.')

    def format_pair(self, pair):
        return pair

    def get_candles(self, *args, **kwargs):
        labels = ['open time', 'open', 'high', 'low', 'close',
                  'volume', 'close time', 'quote asset volume',
                  'number of trades', 'taker buy base asset volume',
                  'taker buy quote asset volume', 'ignore']
        candles = pandas.DataFrame.from_records(self.get_klines(),
                                                columns=labels)
        candles[['open', 'close', 'volume']] = candles[['open', 'close', 'volume']].apply(
            pandas.to_numeric, errors='coerce'
        )
        return candles

    def get_exchange_time(self):
        response = self.client.get_server_time()
        return response['serverTime']

    def get_filters(self):
        if not self.filters:
            self.filters = self.client.get_symbol_info(self.pair)['filters']
        return self.filters

    def get_funds(self, asset='ETH'):
        return float(self.client.get_asset_balance(asset=asset)['free'])

    def get_klines(self, **kwargs):
        return self.client.get_klines(symbol=self.pair,
                                      interval=self.interval,
                                      **kwargs)

    def get_minimum_notional(self):
        filters = self.get_filters()
        min_notional = 0.00000001
        for _filter in filters:
            if _filter['filterType'] == 'MIN_NOTIONAL':
                min_notional = float(_filter['minNotional'])

        return min_notional

    def get_order(self, order_id):
        order = self.client.get_order(symbol=self.pair,
                                      orderId=order_id)
        return order

    def get_order_book(self, limit=5):
        book = self.client.get_order_book(symbol=self.pair,
                                          limit=limit)
        return {'buy': float(book['bids'][0][0]),
                'sell': float(book['asks'][0][0])}

    def get_price_precision(self):
        filters = self.get_filters()
        precision = 0
        for _filter in filters:
            if _filter['filterType'] == 'PRICE_FILTER':
                precision = str(_filter['tickSize']).split('.')[1].find('1') + 1

        return precision

    def get_qty_precision(self):
        filters = self.get_filters()
        precision = 0
        for _filter in filters:
            if _filter['filterType'] == 'LOT_SIZE':
                precision = str(_filter['minQty']).split('.')[1].find('1')+1

        return precision

    def sell_limit(self, quantity, price):
        price = round(price, self.get_price_precision())
        print('Sell Limit - price: {} - quantity: {}'.format(price, quantity))
        order = self.client.order_limit_sell(
            symbol=self.pair,
            quantity=quantity,
            price=price)

        print('Order ID: {}'.format(order['orderId']))
        return order['orderId']

    def sell_market(self):
        order = self.client.order_market_sell(symbol=self.pair,
                                              quantity=self.get_funds(asset=self.weak_coin),
                                              newOrderRespType=self.client.ORDER_RESP_TYPE_FULL)
        amount = sum([(float(op['price']) * float(op['qty'])) for op in order['fills']])
        return round(amount, 8)

    def set_stop_loss(self, buy_price):
        """Set the stop loss with a percentage given in config"""
        raise exceptions.OperationNotAvailable

    def set_stop_loss_limit(self, buy_price):
        """Price in ETH, quantity in 'weak' coin"""
        stop_price = self.get_stop_price(buy_price)
        order = self.client.create_order(
            symbol=self.pair,
            side=enums.SIDE_SELL,
            type=enums.ORDER_TYPE_STOP_LOSS_LIMIT,
            quantity=self.get_funds(asset=self.weak_coin),
            stopPrice=stop_price,
            price=self.get_stop_limit_price(stop_price),
            timeInForce=enums.TIME_IN_FORCE_GTC
        )

        return order['orderId']
