import configparser
import operator
import time

from apis import binance


def get_spread_binance():
    config = configparser.ConfigParser()
    config.read('status.ini')
    exchange = binance.Binance(config, 'a', 1)

    info = exchange.client.get_exchange_info()
    eth_symbol = [s['symbol'] for s in info['symbols'] if s['quoteAsset'] == 'ETH']
    spread = {}

    for symbol in eth_symbol:
        book = exchange.client.get_order_book(symbol=symbol, limit=5)
        buy = float(book['bids'][0][0])
        sell = float(book['asks'][0][0])
        calc = (sell - buy) / sell * 100
        print('{} - {}'.format(symbol, calc))
        spread[symbol] = calc

        time.sleep(0.5)

    # https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
    return sorted(spread.items(), key=operator.itemgetter(1))
