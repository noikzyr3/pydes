"""Test for simple scalping strategy"""
import mock
import unittest

from indicators import sma
from strategies import simple_scalping


class SimpleScalpingTestCase(unittest.TestCase):
    def setUp(self):
        self.candles = ['just', 'for', 'test']
        self.config = {'not': 'important'}
        self.strategy = simple_scalping.SimpleScalpingStrategy(self.candles, self.config)

    def test_sma_init(self):
        self.assertEqual(self.strategy.indicators,
                         {'sma': sma.SimpleMovingAverage})

    @mock.patch('indicators.simple_moving_average.SimpleMovingAverage.execute', autospec=True)
    def test_execute(self, m_execute):
        m_execute.return_value = True
        self.assertTrue(self.strategy.execute())
