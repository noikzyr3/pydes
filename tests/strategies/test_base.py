"""Test for strategy base"""
import mock
import unittest

from strategies import base


class TestMock(mock.Mock):
    def execute(self):
        return True


class TestStrategy(base.StrategyBase):
    indicators = {'test_indicator': TestMock}

    def calculate(self):
        pass


class BaseStrategiesTestCase(unittest.TestCase):
    def setUp(self):
        self.candles = ['just', 'for', 'test']
        self.config = {'not': 'important'}
        self.strategy = TestStrategy(self.candles, self.config)

    def test_strategy_init(self):
        self.assertEqual(self.strategy.dataframe, self.candles)
        self.assertEqual(self.strategy.config, self.config)
        self.assertDictEqual(self.strategy.result, dict())

    @mock.patch('tests.strategies.test_base.TestStrategy.calculate', autospec=True)
    def test_execute(self, m_calculate):
        self.strategy.execute()
        self.assertDictEqual(self.strategy.result, {'test_indicator': True})
        self.assertTrue(m_calculate.called)
