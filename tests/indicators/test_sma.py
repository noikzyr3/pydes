"""Test for indicators"""
from indicators import sma

from . import test_base


class SimpleMovingAverageTestCase(test_base.IndicatorsTestCase):
    def setUp(self):
        super(SimpleMovingAverageTestCase, self).setUp()
        self.sma_indicator = sma.SimpleMovingAverage(self.df, self.config)

    def test_simple_moving_average_function_5(self):
        sma = self.sma_indicator.moving_average(5)
        self.assertEqual(round(sma.values[-1], 8), 0.00108066)

    def test_simple_moving_average_function_8(self):
        sma = self.sma_indicator.moving_average(8)
        self.assertEqual(round(sma.values[-1], 8), 0.00108044)

    def test_simple_moving_average_function_13(self):
        sma = self.sma_indicator.moving_average(13)
        self.assertEqual(round(sma.values[-1], 8), 0.00108092)
