"""Test for indicator's base"""
import configparser
import pandas
import unittest

from indicators import base


class IndicatorsTestCase(unittest.TestCase):
    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('status_test.ini')
        data = pandas.read_csv('tests/fixtures/bncbtc.csv')
        labels = ['open time', 'open', 'high', 'low', 'close', 'volume', 'close time', 'quote asset volume',
                  'number of trades', 'taker buy base asset volume', 'taker buy quote asset volume', 'ignore']
        self.df = pandas.DataFrame(data, columns=labels)


class BaseIndicator(base.BaseIndicator):
    def evaluate(self, *args, **kwargs):
        pass

    def execute(self, *args, **kwargs):
        pass


class IndicatorBaseTestCase(unittest.TestCase):
    def setUp(self):
        self.indicator = BaseIndicator(True, False)

    def test_init(self):
        self.assertTrue(self.indicator.df)
        self.assertFalse(self.indicator.config)
