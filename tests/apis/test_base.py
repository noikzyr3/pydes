import configparser
import mock
import unittest

from apis import base
import constants
import exceptions
import settings


class ApiBase(base.Base):
    def buy_limit(self, quantity, price):
        pass

    def get_candles(self, *args, **kwargs):
        pass

    def format_pair(self, pair):
        return pair

    def set_stop_loss(self):
        pass

    def sell_market(self):
        pass

    def check_order_status_filled(self, *args, **kwargs):
        pass

    def format_interval(self, interval):
        pass

    def buy_market(self):
        pass

    def cancel_order(self, order_id):
        pass

    def get_funds(self, asset='ETH'):
        pass

    def get_order_book(self, *args, **kwargs):
        pass

    def sell_limit(self, quantity, price):
        pass


class ApiBaseTestCase(unittest.TestCase):
    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('status_test.ini')
        self.api = ApiBase(self.config, constants.READY_TO_BUY, 0.05)

    def test_amount__bad_value(self):
        self.config['bot']['buy_mode'] = 'whatever'
        with self.assertRaises(exceptions.BadValueIniFile):
            self.api.amount

    def test_amount__fix(self):
        self.assertEqual(self.api.amount, 0.05)

    @mock.patch('tests.apis.test_base.ApiBase.get_funds', autospec=True)
    def test_amount__allin(self, m_get_funds):
        expected_value = 0.1
        m_get_funds.return_value = expected_value
        self.config['bot']['buy_mode'] = 'allin'
        self.assertEqual(self.api.amount, expected_value)

    @mock.patch('tests.apis.test_base.ApiBase.get_funds', autospec=True)
    def test_amount__already_filled(self, m_get_funds):
        expected_value = 0.3
        self.api._amount = expected_value
        self.assertEqual(self.api.amount, expected_value)
        self.assertFalse(m_get_funds.called)

    def test_start_buy_amount(self):
        self.assertEqual(self.api.buy_amount_start, 0.05)

    def test_min_buy_amount(self):
        self.assertEqual(self.api.buy_amount_min, 0.03)

    def test_max_buy_amount(self):
        self.assertEqual(self.api.buy_amount_max, 0.1)

    def test_buy_percentage(self):
        self.assertEqual(self.api.buy_percentage, 3)

    @mock.patch('tests.apis.test_base.ApiBase.format_interval', autospec=True)
    def test_interval(self, m_format_interval):
        self.api.interval
        m_format_interval.assert_called_once_with(self.api, 1)

    def test_market_price(self):
        self.assertTrue(self.api.market_price)

    def test_max_candle(self):
        self.assertEqual(self.api.max_candle, 100)

    @mock.patch('tests.apis.test_base.ApiBase.format_pair', autospec=True)
    def test_pair(self, m_format_pair):
        self.api.pair
        m_format_pair.assert_called_once_with(self.api, 'XRPETH')

    def test_sell_percentage(self):
        self.assertEqual(self.api.sell_percentage, 3)

    def test_stop_loss_percentage(self):
        self.assertEqual(self.api.stop_loss_percentage, 5)

    @mock.patch('tests.apis.test_base.ApiBase.buy_market', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.set_stop_loss', autospec=True)
    def test_buy_market(self, m_set_stop_loss, m_buy_market):
        self.config['bot']['market_price'] = 'true'
        self.api.buy()

        self.assertTrue(m_set_stop_loss.called)
        self.assertTrue(m_buy_market.called)

    @mock.patch('tests.apis.test_base.ApiBase.get_buy_quantity_price', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.set_stop_loss', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.perform_action_limit', autospec=True)
    def test_buy_limit(self, m_perform_action_limit, m_set_stop_loss, m_get_buy_qty_price):
        self.config['bot']['market_price'] = 'false'
        quantity = 2
        price = 0.1
        m_get_buy_qty_price.return_value = quantity, price
        self.api.buy()

        self.assertTrue(m_set_stop_loss.called)
        self.assertTrue(m_get_buy_qty_price.called)
        m_perform_action_limit.assert_called_once_with(self.api,
                                                       quantity,
                                                       price,
                                                       self.api.buy_limit,
                                                       self.api.buy_market)

    @mock.patch('tests.apis.test_base.ApiBase.get_order_book', autospec=True)
    def test_get_buy_price(self, m_get_order_book):
        book = {'buy': 100, 'sell': 200}
        m_get_order_book.return_value = book
        self.assertEqual(self.api.get_buy_price(), 103)  # buy + buy percentage

    @mock.patch('tests.apis.test_base.ApiBase.get_order_book', autospec=True)
    def test_get_sell_price(self, m_get_order_book):
        book = {'buy': 100, 'sell': 200}
        m_get_order_book.return_value = book
        self.assertEqual(self.api.get_sell_price(), 194)

    @mock.patch('tests.apis.test_base.ApiBase.get_buy_price', autospec=True)
    def test_get_buy_quantity_price(self, m_get_buy_price):
        price = 5
        m_get_buy_price.return_value = price
        with mock.patch('tests.apis.test_base.ApiBase.amount',
                        new_callable=mock.PropertyMock) as m_amount:
            m_amount.return_value = 10
            self.assertTupleEqual(self.api.get_buy_quantity_price(), (2, 5))

    @mock.patch('tests.apis.test_base.ApiBase.get_funds', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.get_sell_price', autospec=True)
    def test_get_sell_quantity_price(self, m_get_sell_price, m_get_funds):
        price = 5
        m_get_sell_price.return_value = price
        m_get_funds.return_value = 10
        self.assertTupleEqual(self.api.get_sell_quantity_price(), (10, 5))

    @mock.patch('tests.apis.test_base.ApiBase.get_buy_price', autospec=True)
    def test_get_stop_price(self, m_get_buy_price):
        m_get_buy_price.return_value = 100
        self.assertEqual(self.api.get_stop_price(), 95)  # Stop loss percentage is 5

    @mock.patch('tests.apis.test_base.ApiBase.cancel_order', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.check_order_status', autospec=True)
    def test_perform_action_limit__in_time(self, m_check_order_status, m_cancel_order):
        settings.limit_timeout_seconds = 20
        settings.limit_check_seconds = 4
        m_check_order_status.side_effect = (False, False, True)
        mock_limit_action = mock.Mock()
        mock_market_action = mock.Mock()
        mock_limit_action.return_value = 123
        qty = 5
        price = 0.5
        self.api.perform_action_limit(qty, price, mock_limit_action, mock_market_action)

        mock_limit_action.assert_called_once_with(qty, price)
        self.assertEqual(m_check_order_status.call_count, 3)
        self.assertFalse(m_cancel_order.called)
        self.assertFalse(mock_market_action.called)

    @mock.patch('tests.apis.test_base.ApiBase.cancel_order', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.check_order_status', autospec=True)
    def test_perform_action_limit__timeout(self, m_check_order_status, m_cancel_order):
        settings.limit_timeout_seconds = 20
        settings.limit_check_seconds = 4
        m_check_order_status.return_value = False
        mock_limit_action = mock.Mock()
        mock_market_action = mock.Mock()
        mock_limit_action.return_value = 123
        qty = 5
        price = 0.5
        self.api.perform_action_limit(qty, price, mock_limit_action, mock_market_action)

        mock_limit_action.assert_called_once_with(qty, price)
        self.assertEqual(m_check_order_status.call_count, 5)
        m_cancel_order.assert_called_once_with(self.api, 123)
        self.assertTrue(mock_market_action.called)

    @mock.patch('tests.apis.test_base.ApiBase.sell_market', autospec=True)
    def test_sell_market(self, m_sell_market):
        self.config['bot']['market_price'] = 'true'
        self.api.sell()
        self.assertTrue(m_sell_market.called)

    @mock.patch('tests.apis.test_base.ApiBase.perform_action_limit', autospec=True)
    @mock.patch('tests.apis.test_base.ApiBase.get_sell_quantity_price', autospec=True)
    def test_sell_limit(self, m_get_sell_qty_price, m_perform_action_limit):
        self.config['bot']['market_price'] = 'false'
        quantity = 2
        price = 0.1
        m_get_sell_qty_price.return_value = quantity, price
        self.api.sell()

        self.assertTrue(m_get_sell_qty_price.called)
        m_perform_action_limit.assert_called_once_with(self.api,
                                                       quantity,
                                                       price,
                                                       self.api.sell_limit,
                                                       self.api.sell_market)
