"""Unittest for binance"""
import configparser
import json
import mock
import unittest

from binance import enums

from apis import binance
import exceptions


class BinanceTestCase(unittest.TestCase):
    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('status_test.ini')
        self.binance = binance.Binance(self.config)

    def get_order(self):
        return {
            "symbol": "LTCBTC",
            "orderId": 1,
            "clientOrderId": "myOrder1",
            "price": "0.1",
            "origQty": "1.0",
            "executedQty": "0.0",
            "status": "NEW",
            "timeInForce": "GTC",
            "type": "LIMIT",
            "side": "BUY",
            "stopPrice": "0.0",
            "icebergQty": "0.0",
            "time": 1499827319559
        }

    @mock.patch('binance.client.Client.order_limit_buy', autospec=True)
    def test_buy_limit(self, m_order_limit_buy):
        expected_id = 123
        m_order_limit_buy.return_value = {
            "symbol": "BTCUSDT",
            "orderId": expected_id,
            "clientOrderId": "6gCrw2kRUAF9CvJDGP16IP",
            "transactTime": 1507725176595,
            "price": "0.00000000",
            "origQty": "10.00000000",
            "executedQty": "10.00000000",
            "status": "FILLED",
            "timeInForce": "GTC",
            "type": "MARKET",
            "side": "SELL"
        }
        qty = 1
        price = 0.1
        self.assertEqual(self.binance.buy_limit(qty, price), expected_id)
        m_order_limit_buy.assert_called_once_with(self.binance.client,
                                                  price=price,
                                                  quantity=qty,
                                                  symbol=self.binance.pair)

    @mock.patch('apis.binance.Binance.calculate_quantity_buy_market_price', autospec=True)
    @mock.patch('binance.client.Client.order_market_buy', autospec=True)
    def test_buy_market(self, m_order_market_buy, m_calculate):
        qty = 0.1
        m_calculate.return_value = qty
        self.binance.buy_market()
        m_order_market_buy.assert_called_once_with(self.binance.client,
                                                   symbol=self.binance.pair,
                                                   quantity=qty)

    @mock.patch('apis.binance.Binance.get_order_book', autospec=True)
    def test_calculate_quantity_buy_market_price(self, m_book):
        with mock.patch('apis.binance.Binance.amount',
                        new_callable=mock.PropertyMock) as m_amount:
            m_amount.return_value = 10
            m_book.return_value = {'sell': 2}
            self.assertEqual(self.binance.calculate_quantity_buy_market_price(), 5)

    @mock.patch('binance.client.Client.cancel_order', autospec=True)
    def test_cancel_order(self, m_cancel_order):
        order_id = 123
        self.binance.cancel_order(order_id)
        m_cancel_order.assert_called_once_with(self.binance.client,
                                               symbol=self.binance.pair,
                                               orderId=order_id)

    @mock.patch('binance.client.Client.get_order', autospec=True)
    def test_check_order_status__true(self, m_get_order):
        order = self.get_order()
        order['status'] = enums.ORDER_STATUS_FILLED
        m_get_order.return_value = order
        self.assertTrue(self.binance.check_order_status_filled(123))

    @mock.patch('binance.client.Client.get_order', autospec=True)
    def test_check_order_status__false(self, m_get_order):
        order = self.get_order()
        m_get_order.return_value = order
        self.assertFalse(self.binance.check_order_status_filled(123))

    @mock.patch('binance.client.Client.get_order', autospec=True)
    def test_check_order_status__exception(self, m_get_order):
        order = self.get_order()
        order['status'] = enums.ORDER_STATUS_REJECTED
        m_get_order.return_value = order
        self.assertRaises(exceptions.UnexpectedOrderStatus,
                          self.binance.check_order_status_filled,
                          123)

    def test_format_interval__good(self):
        intervals = [(1, enums.KLINE_INTERVAL_1MINUTE),
                     (3, enums.KLINE_INTERVAL_3MINUTE),
                     (5, enums.KLINE_INTERVAL_5MINUTE),
                     (15, enums.KLINE_INTERVAL_15MINUTE),
                     (30, enums.KLINE_INTERVAL_30MINUTE),
                     (60, enums.KLINE_INTERVAL_1HOUR)]
        for interval in intervals:
            self.assertEqual(self.binance.format_interval(interval[0]), interval[1])

    def test_format_interval__bad(self):
        self.assertRaises(exceptions.BadValueIniFile,
                          self.binance.format_interval,
                          'whatever')

    def test_format_pair(self):
        pair = 'ETHXRP'
        self.assertEqual(self.binance.format_pair(pair), pair)

    @mock.patch('pandas.core.frame.DataFrame.from_records')
    @mock.patch('apis.binance.Binance.get_klines', autospec=True)
    def test_get_candles(self, m_get_klines, m_pandas):
        m_get_klines.return_value = 'whatever'
        m_pandas.return_value = True
        self.binance.get_candles()
        self.assertTrue(m_get_klines.called)
        self.assertTrue(m_pandas.called)

    def test_get_exchange_time(self):
        self.assertTrue(isinstance(self.binance.get_exchange_time(), int))

    @mock.patch('binance.client.Client.get_asset_balance', autospec=True)
    def test_get_funds(self, m_get_funds):
        m_get_funds.return_value = 1
        self.assertEqual(self.binance.get_funds('BTC'), 1)
        m_get_funds.assert_called_once_with(self.binance.client, 'BTC')

    @mock.patch('binance.client.Client.get_klines', autospec=True)
    def test_get_klines(self, m_get_klines):
        self.binance.get_klines()
        m_get_klines.assert_called_once_with(self.binance.client,
                                             symbol=self.binance.pair,
                                             interval=self.binance.interval)

    @mock.patch('binance.client.Client.get_order_book', autospec=True)
    def test_get_order_book(self, m_get_order_book):
        self.binance.get_order_book()
        m_get_order_book.assert_called_once_with(self.binance.client,
                                                 symbol=self.binance.pair,
                                                 limit=5)

    @mock.patch('binance.client.Client.order_limit_sell', autospec=True)
    def test_sell_limit(self, m_order_limit_sell):
        qty = 1
        price = 10
        self.binance.sell_limit(qty, price)
        m_order_limit_sell.assert_called_once_with(self.binance.client,
                                                   symbol=self.binance.pair,
                                                   quantity=qty,
                                                   price=price)

    @mock.patch('apis.binance.Binance.get_funds', autospec=True)
    @mock.patch('binance.client.Client.order_market_sell', autospec=True)
    def test_sell_market(self, m_order_market_sell, m_get_funds):
        m_get_funds.return_value = 10
        self.binance.sell_market()
        m_order_market_sell.assert_called_once_with(self.binance.client,
                                                    symbol=self.binance.pair,
                                                    quantity=m_get_funds.return_value)

    @mock.patch('apis.binance.Binance.get_funds', autospec=True)
    @mock.patch('apis.base.Base.get_stop_price', autospec=True)
    @mock.patch('binance.client.Client.create_order', autospec=True)
    def test_set_stop_loss(self, m_create_order, m_stop_price, m_get_funds):
        m_stop_price.return_value = 5
        m_get_funds.return_value = 10
        self.binance.set_stop_loss()
        m_create_order.assert_called_once_with(self.binance.client,
                                               symbol=self.binance.pair,
                                               timeInForce=enums.TIME_IN_FORCE_GTC,
                                               side=enums.SIDE_SELL,
                                               type=enums.ORDER_TYPE_STOP_LOSS,
                                               quantity=m_get_funds.return_value,
                                               stopPrice=m_stop_price.return_value)
