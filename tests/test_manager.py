import configparser
import unittest

import manager


class ManagerTestCase(unittest.TestCase):
    def setUp(self):
        config = configparser.ConfigParser()
        config.read('status_test.ini')
        self.manager = manager.Manager(config)

    def test_get_headers(self):
        self.assertDictEqual(self.manager.security_headers(), {'license': 'license'})
