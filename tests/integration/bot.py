"""Integration Tests"""
import unittest

import bot


class IntegrationTestCase(unittest.TestCase):
    def setUp(self):
        self.bot = bot.BotController()

    def test_bot_execution(self):
        self.bot.run()
