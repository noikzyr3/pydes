"""Integration Tests"""
import configparser
import unittest

import manager
from apis import binance


class IntegrationTestCase(unittest.TestCase):
    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('status.ini')

        self.manager = manager.Manager(self.config)
        self.binance = binance.Binance(self.config,
                                       self.manager.get_status(),
                                       self.manager.get_funds())

    def test_buy(self):
        self.binance.buy()

    def test_sell(self):
        self.binance.sell()
