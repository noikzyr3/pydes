"""Tests for bot controller"""
import mock
import unittest

from apis import binance
import bot
import constants
import exceptions
from strategies import simple_scalping


class BotControllerTestCase(unittest.TestCase):
    def setUp(self):
        self.filename = 'status_test.ini'
        self.status = False
        self.bot = bot.BotController(config_file=self.filename)

    @mock.patch('manager.Manager.check_security', autospec=True)
    @mock.patch('manager.Manager.check_benefits', autospec=True)
    @mock.patch('bot.BotController.check_status', autospec=True)
    @mock.patch('apis.binance.Binance.get_candles', autospec=True)
    @mock.patch('strategies.simple_scalping.SimpleScalpingStrategy.execute', autospec=True)
    @mock.patch('bot.BotController.execute_action', autospec=True)
    def test_run(self,
                 m_execute_action,
                 m_strategy_execute,
                 m_get_candles,
                 m_check_status,
                 m_check_benefits,
                 m_check_security):
        m_get_candles.return_value = {'not': 'important'}
        m_strategy_execute.return_value = constants.BUY

        self.bot.run()

        m_check_security.assert_called_once_with(self.bot.manager,
                                                 '',
                                                 self.bot.config['bot']['license'])
        m_check_benefits.assert_called_once_with(self.bot.manager)
        m_check_status.assert_called_once_with(self.bot)
        m_get_candles.assert_called_once_with(self.bot.exchange)
        m_execute_action.assert_called_once_with(self.bot, constants.BUY)

    def test_get_exchange_normal__good(self):
        self.bot.config['bot']['simulation'] = 'false'
        self.assertIsInstance(self.bot.get_exchange_normal(), binance.Binance)

    def test_get_exchange_normal__bad(self):
        self.bot.config['bot']['exchange'] = 'whatever'
        self.bot.config['bot']['simulation'] = 'false'
        self.assertRaises(exceptions.BadValueIniFile,
                          self.bot.get_exchange_normal)

    def test_get_strategy__good(self):
        self.assertIsInstance(self.bot.get_strategy(True, False),
                              simple_scalping.SimpleScalpingStrategy)

    def test_get_strategy__bad(self):
        self.bot.config['bot']['strategy'] = 'whatever'
        self.assertRaises(exceptions.BadValueIniFile,
                          self.bot.get_strategy,
                          True,
                          False)

    @mock.patch('manager.Manager.update_status', autospec=True)
    @mock.patch('bot.BotController.perform_buy', autospec=True)
    @mock.patch('bot.BotController.perform_sell', autospec=True)
    def test_execute_action__buy(self, m_perform_sell, m_perform_buy, m_update_status):
        self.bot.execute_action(constants.BUY)
        self.assertFalse(m_perform_sell.called)
        self.assertTrue(m_perform_buy.called)
        m_update_status.assert_called_once_with(self.bot.manager,
                                                constants.WAITING_FOR_SELL)

    @mock.patch('manager.Manager.update_status', autospec=True)
    @mock.patch('bot.BotController.perform_buy', autospec=True)
    @mock.patch('bot.BotController.perform_sell', autospec=True)
    def test_execute_action__sell(self, m_perform_sell, m_perform_buy, m_update_status):
        self.bot.execute_action(constants.SELL)
        self.assertTrue(m_perform_sell.called)
        self.assertFalse(m_perform_buy.called)
        m_update_status.assert_called_once_with(self.bot.manager,
                                                constants.READY_TO_BUY)

    @mock.patch('manager.Manager.update_status', autospec=True)
    @mock.patch('bot.BotController.perform_buy', autospec=True)
    @mock.patch('bot.BotController.perform_sell', autospec=True)
    def test_execute_action__noop(self, m_perform_sell, m_perform_buy, m_update_status):
        self.bot.execute_action(constants.NOOP)
        self.assertFalse(m_perform_buy.called)
        self.assertFalse(m_perform_sell.called)
        self.assertFalse(m_update_status.called)

    @mock.patch('apis.binance.Binance.buy', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_perform_buy__ok(self, m_get_status, m_buy):
        m_get_status.return_value = constants.READY_TO_BUY
        self.bot.perform_buy()
        self.assertTrue(m_buy.called)

    @mock.patch('apis.binance.Binance.buy', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_perform_buy__noop(self, m_get_status, m_buy):
        m_get_status.return_value = constants.WAITING_FOR_SELL
        self.bot.perform_buy()
        self.assertFalse(m_buy.called)

    @mock.patch('apis.binance.Binance.sell', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_perform_sell__ok(self, m_get_status, m_sell):
        m_get_status.return_value = constants.WAITING_FOR_SELL
        self.bot.perform_sell()
        self.assertTrue(m_sell.called)

    @mock.patch('apis.binance.Binance.sell', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_perform_sell__noop(self, m_get_status, m_sell):
        m_get_status.return_value = constants.READY_TO_BUY
        self.bot.perform_sell()
        self.assertFalse(m_sell.called)

    def test_perform_noop(self):
        self.assertIsNone(self.bot.perform_noop())

    @mock.patch('apis.binance.Binance.get_funds', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_check_status__ok(self, m_get_status, m_get_funds):
        m_get_funds.return_value = 1
        m_get_status.return_value = constants.READY_TO_BUY
        self.assertIsNone(self.bot.check_status())

    @mock.patch('apis.binance.Binance.get_funds', autospec=True)
    @mock.patch('manager.Manager.get_status', autospec=True)
    def test_check_status__exception(self, m_get_status, m_get_funds):
        m_get_funds.return_value = 0.001
        m_get_status.return_value = constants.READY_TO_BUY
        self.assertRaises(exceptions.InsufficientFunds,
                          self.bot.check_status)
