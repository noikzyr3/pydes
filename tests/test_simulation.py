"""Simulation Test for bot based in data samples"""
import configparser
import pandas
import unittest

from apis import binance
import bot
import constants
import manager


SIM_BUY = 'simulation buy'
SIM_SELL = 'simulation sell'


class BinanceSimulationTest(binance.Binance):
    def __init__(self, config):
        super(BinanceSimulationTest, self).__init__(config)
        data = pandas.read_csv('tests/fixtures/dnteth_bearish.csv')
        labels = ['open time', 'open', 'high', 'low', 'close', 'volume', 'close time', 'quote asset volume',
                  'number of trades', 'taker buy base asset volume', 'taker buy quote asset volume', 'ignore']
        self.df = pandas.DataFrame(data, columns=labels)
        self.candle_count = None
        self.candle_max = 100

    def buy_limit(self, quantity, price):
        pass

    def buy_market(self):
        return SIM_BUY

    def get_candles(self, *args, **kwargs):
        return self.df[0:self.candle_max + self.candle_count]

    def sell_limit(self, quantity, price):
        pass

    def sell_market(self):
        return SIM_SELL

    def set_stop_loss(self):
        print('stop loss set at: {}'.format(self.get_stop_price()))

    def check_order_status_filled(self, order_id):
        return True


class BotControllerSimulation(bot.BotController):
    def __init__(self, config):
        self.config = config
        self.manager = manager.ManagerSimulation()
        self.exchange = self.get_exchange()
        self.status = constants.READY_TO_BUY
        self.funds = 0.26  # 100eur aprox
        self.qty = 0
        self.step = 0

    def get_price(self):
        candles = self.exchange.get_candles()
        return self.exchange.get_candles()['close'][len(candles) - 1]

    def calc_buy_quantity(self, price):
        return round(self.funds / price, 8)

    def perform_buy(self):
        if self.status == constants.READY_TO_BUY:
            price = self.get_price()
            self.qty = self.calc_buy_quantity(price)
            self.funds -= round(price * self.qty, 8)
            self.status = constants.WAITING_FOR_SELL
            print('#{} Buy {} DNT for {} ETH each'.format(self.step, self.qty, price))
            print('Current funds: {}'.format(self.funds))
        else:
            self.perform_noop()

    def perform_sell(self):
        if self.status == constants.WAITING_FOR_SELL:
            price = self.get_price()
            self.status = constants.READY_TO_BUY
            self.funds += round(price * self.qty, 8)
            print('#{} Sell {} DNT for {} ETH each'.format(self.step, self.qty, price))
            print('Current funds: {}'.format(self.funds))
        else:
            self.perform_noop()

    def perform_noop(self):
        pass

    def get_exchange(self):
        return BinanceSimulationTest(self.config)

    def check_status(self):
        pass


class SimulationTests(unittest.TestCase):
    def setUp(self):
        self.config = configparser.ConfigParser()
        self.config.read('status.ini')

    def test_simulation(self):
        print('-')
        # self.config['bot']['strategy'] = 'jeremias'
        self.bot = BotControllerSimulation(self.config)
        for n in range(900):
            self.bot.step = n
            self.bot.exchange.candle_count = n
            self.bot.run()
