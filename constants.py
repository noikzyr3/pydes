"""Constants file"""

# Actions
BUY = 'buy'
SELL = 'sell'
NOOP = 'noop'

# Indicator types
MOMENTUM = 'momentum'
TREND = 'trend'
VOLATILITY = 'volatility'
STUDIES = 'studies'

# Status
READY_TO_BUY = 'ready to buy'
WAITING_FOR_SELL = 'waiting for sell'
